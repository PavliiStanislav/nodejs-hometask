const { Router } = require('express');
const FightService = require('../services/fightService');
const { createUserValid, updateUserValid } = require('../middlewares/user.validation.middleware');
const { responseMiddleware } = require('../middlewares/response.middleware');


const router = Router();

router.post('/', createUserValid, (req, res, next) => {   
  try {       
      const data = FightService.create(req.body)
      res.data = data;        
  }    
  catch (err) {       
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.get('/', (req, res, next) => {
  try {
      const data = FightService.searchAll()
      res.data = data;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.get('/:id', (req, res, next) => {
  try {
      const id = req.params.id

      const data = FightService.searchOne(id)
      res.data = data;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.put('/:id', updateUserValid, (req, res, next) => {
  try {
      const id = req.params.id
      const data = FightService.update(id, req.body)

      res.data = data;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

router.delete('/:id', (req, res, next) => {
  try {
      const id = req.params.id
      const data = FightService.delete(id)
      res.data = data;
  } catch (err) {
      res.err = err;
  } finally {
      next();
  }
}, responseMiddleware);

// OPTIONAL TODO: Implement route controller for fights

module.exports = router;