
## Запуск проекта
* **При первом запуске** необходимо выполнить 
```
. build-start.sh
```
(исполняемый файл `build-start.sh` находится в корне проекта)
* Далее можно запускать с помощью команды
```
npm start
```